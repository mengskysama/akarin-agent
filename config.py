DELUGE_URL = 'http://127.0.0.1:8113/json'
DELUGE_PASS = 'mengsky233'

DOWNLOAD_PATH = '/data/downloads'
COMPLETED_PATH = '/data/completed'
BACKUP_TMP_PATH = '/tmp/akarin'

DB = {
    'host': '104.143.10.220',
    'username': 'root',
    'port': 3306,
    'password': 'root_797979',
    'db': 'akarin',
    'timeout': 30
}

BACKUP_CO = 2
BACKUP_ACCOUNTS = [
    'cache00',
    'cache01',
    'cache02'
]

DOWNLOAD_DEAD_TIME_HOUR = 24 * 4

CLEANUP_DISK_SPACE = 500 * 1024 * 1024 * 1024

RAM_DISK_ENABLE = True
RAM_DISK_SIZE = 10
RAM_DISK_BACKUP_TMP_PATH = '/ramdisk/akarin'
