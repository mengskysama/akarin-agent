from gevent.subprocess import Popen, PIPE
import os
import logging
import time
import config

logging.basicConfig(level=logging.INFO, format='[%(filename)s:%(lineno)s - %(funcName)s ] %(message)s')


def _call(cmd, cwd):
    child = Popen(cmd, shell=True, cwd=cwd, stdout=PIPE, stderr=PIPE)
    result = child.communicate()[0]
    code = child.returncode
    return result, code


def now_time_str():
    return time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())


class RARArchive(object):

    @classmethod
    def compact(cls, src_dir: str, target_file: str) -> bool:
        """
        打包文件夹
        :param src_dir:
        :param target_file:
        :return:
        """
        if os.path.isfile(target_file):
            os.remove(target_file)
        target_dir = os.path.dirname(target_file)
        if not os.path.exists(target_dir):
            os.makedirs(target_dir)
        src_dir, src_name = os.path.split(src_dir)
        cmd = 'nice -n -20 ionice -c3 rar -m0 a -r -y {0!r} {1!r}'.format(target_file, src_name)
        result, code = _call(cmd, src_dir)
        return code == 0


class Rclone(object):

    @classmethod
    def upload(cls, src: str, dst: str, config_name: str, mode: str) -> bool:
        child = Popen(
            ['rclone', mode, src, '{}:{}'.format(config_name, dst),
             '--retries', '999', '-v', '--retries-sleep=10s',
             '--bwlimit', '25M'], stdout=PIPE, stderr=PIPE, env=os.environ)
        while child.poll() is None:
            r = child.stderr.readline()
            logging.info('Rclone {}'.format(r.strip().decode('utf-8')))
        return child.returncode == 0


def create_ram_disk():
    os.system('mkdir {}'.format(config.RAM_DISK_BACKUP_TMP_PATH))
    os.system('mount -t tmpfs -o size={}G tmpfs {}'.
              format(config.RAM_DISK_SIZE, config.RAM_DISK_BACKUP_TMP_PATH))

# print(Rclone.upload('/tmp/test', '/tmp/test', 'g', 'move'))
# print(RARArchive.compact('/tmp/test', '/tmp/a.rar'))
