from gevent import monkey

monkey.patch_socket()
from gevent.lock import BoundedSemaphore
import config
import logging
import gnsq
import pymysql
import gevent
import psutil
import time
import json
import os
import random
from dal.task import TaskModel
from cli.deluge import DelugeClient
from utils.utils import RARArchive, Rclone, now_time_str, create_ram_disk

logging.basicConfig(level=logging.INFO)


class Agent(object):

    def __init__(self):
        self._deluge_cli = DelugeClient(config.DELUGE_URL, config.DELUGE_PASS)
        self._producer = gnsq.Producer('localhost:4150')
        self._completed_consumer = gnsq.Consumer('torrent_download_completed', '_completed_task', 'localhost:4150',
                                                 message_handler=self._completed_task, max_tries=0)
        self._backup_consumer0 = gnsq.Consumer('torrent_download_completed', '_backup_task', 'localhost:4150',
                                               message_handler=self._backup_task, max_tries=0)
        self._backup_consumer1 = gnsq.Consumer('torrent_download_completed', '_backup_task', 'localhost:4150',
                                               message_handler=self._backup_task, max_tries=0)
        self._backup_consumer2 = gnsq.Consumer('torrent_download_completed', '_backup_task', 'localhost:4150',
                                               message_handler=self._backup_task, max_tries=0)
        self._sem_packing = BoundedSemaphore(1)
        self._sem_backup = BoundedSemaphore(config.BACKUP_CO)
        self._processing = {}

    def _mon_download_completed_task(self):
        """
        找到完成的种子并移动到完成目录
        :return:
        """
        while True:
            try:
                res = self._deluge_cli.get_torrents()
                for info_hash in res:
                    torrent = res[info_hash]
                    if torrent['total_size'] != torrent['total_done']:
                        continue
                    # todo except
                    self._deluge_cli.remove_torrent(info_hash, False)
                    gevent.sleep(1)
                    src = os.path.join(config.DOWNLOAD_PATH, info_hash)
                    dst = os.path.join(config.COMPLETED_PATH)
                    os.system('mv {} {}'.format(src, dst))
                    data = json.dumps({
                        'info_hash': info_hash,
                        'state': 'completed',
                        'name': torrent['name'],
                        'total_size':  torrent['total_size'],
                    }).encode('utf-8')
                    self._producer.publish('torrent_download_completed', data)
                    logging.info('info_hash:{} move to completed dir'.format(info_hash))
            except Exception as e:
                logging.warn('mon download completed torrent failed: {}'.format(e))
            finally:
                gevent.sleep(10)

    def run(self):
        t = [
            gevent.spawn(self._producer.start),
            gevent.spawn(self._completed_consumer.start),
            gevent.spawn(self._backup_consumer0.start),
            gevent.spawn(self._mon_download_completed_task),
            gevent.spawn(self._loop_clean_up),
        ]
        if config.RAM_DISK_ENABLE:
            create_ram_disk()
        else:
            t.append(gevent.spawn(self._backup_consumer1.start))
            t.append(gevent.spawn(self._backup_consumer2.start))
        gevent.joinall(t)

    @classmethod
    def _completed_task(cls, consumer, message):
        """
        更新任务状态为完成
        :param consumer:
        :param message:
        :return:
        """
        data = json.loads(message.body)
        info_hash = data['info_hash']
        try:
            TaskModel().update_task({'state': 'completed'}, info_hash)
            logging.warn('info_hash {} update state success'.format(info_hash))
        except Exception as e:
            logging.warn('info_hash {} update state failed: {}'.format(info_hash, e))
            message.requeue()

    @staticmethod
    def _hang_msg(message):
        """
        保持消息不让nsq重试
        :param message:
        :return:
        """
        info_hash = json.loads(message.body)['info_hash']
        while True:
            gevent.sleep(50)
            logging.info('touch info_hash: {}'.format(info_hash))
            message.touch()

    def _backup_task(self, consumer, message):
        """
        打包任务到google,回写数据库
        max_in_flight = 2
        :param consumer:
        :param message:
        :return:
        """
        msg = json.loads(message.body)
        info_hash = msg['info_hash']
        if info_hash in self._processing:
            logging.info('info_hash:{} race condition exit'.format(info_hash))
            return
        self._processing[info_hash] = time.time()
        src_dir = os.path.join(config.COMPLETED_PATH, info_hash)

        if 'total_size' in msg:
            total_size = msg['total_size']
        else:
            total_size = sum(os.path.getsize(f) for f in os.listdir(src_dir) if os.path.isfile(f))
        if total_size > config.RAM_DISK_SIZE * 1024 * 1024 * 1024 - 200 * 1024 * 1024:
            # out of limit
            archive_file = os.path.join(config.BACKUP_TMP_PATH, '{}.rar'.format(info_hash))
        else:
            # clean up ram disk
            os.system('rm -rf {}/*'.format(config.RAM_DISK_BACKUP_TMP_PATH))
            archive_file = os.path.join(config.RAM_DISK_BACKUP_TMP_PATH, '{}.rar'.format(info_hash))

        # 同时只有1个打包任务
        g = gevent.spawn(self._hang_msg, message)
        self._sem_packing.acquire()

        err = False
        requeue = False
        try:
            # migrate
            if TaskModel().get_cache_by_info_hash(info_hash) is not None:
                raise Exception('cache exits ignore')
            else:
                if not os.path.exists(src_dir):
                    raise Exception('{} not exists'.format(src_dir))
                logging.info('info_hash:{} begin archive'.format(info_hash))
                RARArchive.compact(src_dir, archive_file)
        except pymysql.MySQLError as e:
            logging.error('db error: {}'.format(e))
            requeue = True
            err = True
        except Exception as e:
            logging.error('info_hash:{} create RARArchive failed: {}'.format(info_hash, e))
            err = True
        finally:
            self._sem_packing.release()
        if err:
            g.kill()
            if requeue:
                message.requeue()
            del self._processing[info_hash]
            return

        # 同时只有2个上传任务
        self._sem_backup.acquire()
        cache_dir = '/cache/{}/{}/'.format(info_hash[:2], info_hash[2:4])
        account = random.choice(config.BACKUP_ACCOUNTS)
        try:
            if not Rclone.upload(archive_file, cache_dir, account, 'move'):
                raise Exception('upload failed exit code != 0')
        except Exception as e:
            # todo delete file or alert?
            logging.error('info_hash:{} upload failed: {}'.format(info_hash, e))
            err = True
        finally:
            self._sem_backup.release()
        if err:
            g.kill()
            del self._processing[info_hash]
            return

        # 上传完成，回写数据库,一直写直到成功
        while True:
            try:
                now = now_time_str()
                TaskModel().insert_cache({
                    'info_hash': info_hash,
                    'name': msg['name'],
                    '`key`': account,
                    'create_time': now,
                    'last_hit_time': now,
                    'created_at': now,
                    'updated_at': now
                })
                break
            except Exception as e:
                logging.warn('info_hash:{} insert db.cache failed: {} retry ...'.format(info_hash, e))
                gevent.sleep(30)

        g.kill()
        del self._processing[info_hash]

    def _clean_up(self):
        """
        清理任务
        :return:
        """

        # todo 和添加任务要锁

        def delete_task(info_hash_lst):
            if len(info_hash_lst) == 0:
                return
            # 回写数据库,一直写直到成功
            # todo 不能保证一定成功，数据库要补偿操作
            while True:
                try:
                    TaskModel().delete_tasks(info_hash_lst)
                    break
                except Exception as ex:
                    logging.warn('cleanup deluge update db.tasks failed: {} retry ...'.format(ex))
                    gevent.sleep(60)

        # cleanup deluge 下载超过deadTime
        # todo hard code dead time 48h
        dead_time = time.time() - config.DOWNLOAD_DEAD_TIME_HOUR * 3600
        logging.info('cleanup deluge begin ...')
        deleted = []
        try:
            all_torrents = self._deluge_cli.get_torrents()
            expire_torrents = list(
                filter(lambda x: x['time_added'] < dead_time and x['progress'] < 1, all_torrents.values()))
            print(expire_torrents)
            for t in expire_torrents:
                logging.info('info_hash: {} deadline cleanup ...'.format(t['hash']))
                self._deluge_cli.remove_torrent(t['hash'], True)
                deleted.append(t['hash'])
                # wait disk delete
                gevent.sleep(30)
        except Exception as e:
            print(e)
            logging.warn('cleanup deluge failed: {}'.format(e))

        # todo 前端任务标记为删除
        # 临时暴力删除，从早到新删除
        free = psutil.disk_usage(config.DOWNLOAD_PATH).free
        if not free < config.CLEANUP_DISK_SPACE:
            delete_task(deleted)
            return

        logging.info('free {} cleanup expire begin ...'.format(free))
        try:
            all_torrents = self._deluge_cli.get_torrents()
            expire_tasks = TaskModel().get_task_by_create_time()
            hit = 0
            for task in expire_tasks:
                if hit == 5:
                    break
                info_hash = task['info_hash']
                if info_hash in deleted:
                    continue
                task_path = os.path.join(config.COMPLETED_PATH, info_hash)
                if os.path.exists(task_path):
                    hit += 1
                    logging.info('info_hash: {} cleanup ...'.format(task['info_hash']))
                    os.system("rm -rf {}".format(task_path))
                    gevent.sleep(30)
                if info_hash in all_torrents:
                    hit += 1
                    logging.info('info_hash: {} deadline cleanup ...'.format(task['info_hash']))
                    self._deluge_cli.remove_torrent(task['info_hash'], True)
                    deleted.append(task['info_hash'])
        except Exception as e:
            logging.warn('cleanup expire failed: {}'.format(e))

        delete_task(deleted)

    def _loop_clean_up(self):
        while True:
            self._clean_up()
            gevent.sleep(60)


Agent().run()
