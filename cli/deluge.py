import requests

STATUS_KEY = [
    "download_payload_rate",
    "upload_payload_rate",
    "eta",
    "hash",
    "name",
    "num_peers",
    "num_seeds",
    "progress",
    "total_size",
    "total_done",
    "state",
    "time_added"
]


class DelugeClient(object):

    def __init__(self, url, password):
        self._http_cli = requests.session()
        self._req_id = 0
        self._url = url
        self._login(password)

    @property
    def req_id(self):
        self._req_id += 1
        return self._req_id

    def _request(self, method: str, params: object) -> dict:
        data = {
            'method': method,
            'id': self.req_id,
            'params': params,
        }
        r = self._http_cli.post(self._url, json=data)
        return r.json()

    def _login(self, password):
        self._request('auth.login', [password])

    def get_torrents(self) -> dict:
        return self._request('core.get_torrents_status', [{}, STATUS_KEY])['result']

    def remove_torrent(self, info_hash, delete_data: bool):
        return self._request('core.remove_torrent', [info_hash, delete_data])


if __name__ == '__main__':
    d = DelugeClient('http://176.9.73.149:8113/json', 'mengsky233')
    print(d.get_torrents())

