import gevent.monkey

gevent.monkey.patch_all()
import config
import pymysql


def get_conn():
    return pymysql.connect(host=config.DB['host'],
                           user=config.DB['username'],
                           passwd=config.DB['password'],
                           port=config.DB['port'],
                           db=config.DB['db'],
                           cursorclass=pymysql.cursors.DictCursor)


class TaskModel(object):
    _instance = None

    def __new__(cls, *args, **kw):
        if cls._instance is None:
            cls._instance = object.__new__(cls, *args, **kw)
        return cls._instance

    def __init__(self):
        pass

    def update_task(self, data, info_hash: str) -> bool:
        conn = get_conn()
        with conn.cursor() as cursor:
            sql = 'UPDATE tasks SET {}'.format(', '.join('{}=%s'.format(k) for k in data)) + \
                  ' WHERE info_hash="%s"' % info_hash
            cursor.execute(sql, list(data.values()))
        conn.commit()
        conn.close()

    def insert_cache(self, data):
        conn = get_conn()
        with conn.cursor() as cursor:
            placeholders = ', '.join(['%s'] * len(data))
            columns = ', '.join(data.keys())
            sql = "INSERT INTO %s ( %s ) VALUES ( %s )" % ('caches', columns, placeholders) + \
                  ' ON DUPLICATE KEY UPDATE id=id'
            cursor.execute(sql, list(data.values()))
        conn.commit()
        conn.close()

    def delete_tasks(self, info_hash: list):
        conn = get_conn()
        with conn.cursor() as cursor:
            placeholders = ', '.join(['%s'] * len(info_hash))
            sql = "DELETE FROM tasks where info_hash IN ( %s )" % placeholders
            cursor.execute(sql, info_hash)
        conn.commit()
        conn.close()

    def get_task_by_create_time(self) -> list:
        conn = get_conn()
        with conn.cursor() as cursor:
            sql = "select Distinct(`info_hash`) , `name`, max(`create_time`) as create_time " \
                  "from tasks group by `info_hash` ORDER BY create_time"
            cursor.execute(sql)
            results = cursor.fetchall()
        conn.commit()
        conn.close()
        return list(results)

    def get_cache_by_info_hash(self, info_hash: str) -> dict:
        conn = get_conn()
        with conn.cursor() as cursor:
            sql = "select * from caches WHERE info_hash = %s"
            cursor.execute(sql, info_hash)
            results = cursor.fetchall()
        conn.commit()
        conn.close()
        return None if len(results) == 0 else results[0]
