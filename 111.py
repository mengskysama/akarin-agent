import gevent
from gevent.event import AsyncResult
import gnsq
import json
import logging
n = 0

def _hang_msg(message):
    """
    保持消息不让nsq重试
    :param message:
    :return:
    """
    while True:
        print('hang' + json.loads(message.body)['info_hash'])
        gevent.sleep(15)
        message.touch()


def test(c, message):
    print(message.body)
    g = gevent.spawn(_hang_msg, message)
    gevent.sleep(650)
    g.kill()

    global n
    n += 1
    # data = json.dumps({
    #     'info_hash': '{}'.format(n),
    #     'state': 'completed',
    #     'name': '222'
    # }).encode('utf-8')
    # _producer.publish('test', data)


_producer = gnsq.Producer('localhost:4150')
_producer.start()
data = json.dumps({
    'info_hash': 'iii',
    'state': 'completed',
    'name': '222'
}).encode('utf-8')
_producer.publish('test', data)
# data = json.dumps({
#     'info_hash': 'jjj',
#     'state': 'completed',
#     'name': '222'
# }).encode('utf-8')
# _producer.publish('test', data)

c1 = gnsq.Consumer('test', 'test', 'localhost:4150', message_handler=test, max_tries=0)
c2 = gnsq.Consumer('test', 'test', 'localhost:4150', message_handler=test, max_tries=0)

gevent.joinall([gevent.spawn(c1.start), gevent.spawn(c2.start)])


# 790f393f26a6b05786adaed4ae39d7dbfb43b805.rar: Failed to copy: googleapi: Error 403: User rate limit exceeded., userRateLimitExceeded